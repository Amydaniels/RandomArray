﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomArray
{
    class Program
    {
        static void Main(string[] args)
        {
            //Generate random numbers in into an array list
            //makes sure the random numbers are unique
            ArrayList myRandNumb = new ArrayList();

            Random rand = new Random();
            Stopwatch sw1 = new Stopwatch();
            sw1.Start();

            for (int i = 0; i < 1000; i++)
            {
                //Generate random number and check if it exists in the arraylist, if it exists, generate new one until it doesn't exist
                bool exists = true;
                int num;
                while (exists)
                {
                    num = rand.Next(10000, 99999999);
                    if (!myRandNumb.Contains(num))
                    {
                        exists = false;
                        myRandNumb.Add(num);
                    }
                }
            }
            foreach (int num in myRandNumb)
            {
                Console.Write(" " + num);
            }

            Console.WriteLine("");

            Console.WriteLine("ArrayList used: " + sw1.Elapsed);

            Console.ReadLine();




        }
    }
}
